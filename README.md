# Icon-OSM

Icon-OSM is an icons pack that we use on [the Open Science MOOC website](https://opensciencemooc.eu/). It is 
based on [Beautiful Flat Icons pack by Elegant Themes][bfi].

[bfi]: https://www.elegantthemes.com/blog/freebie-of-the-week/beautiful-flat-icons-for-free
